---
title: "A new Control Flow Graph representation for Frama-C"
author: Valentin Perrelle
date: April 9, 2018
header-includes:
- \usepackage{tikz}
- \usepackage{macros}
- \usetikzlibrary{chains,interpretedautomata,backgrounds,calc}
- \renewcommand{\ttdefault}{pcr}
- \lstset{
    escapechar=^,
    basicstyle=\footnotesize\ttfamily,
    keywordstyle=\bfseries,
    columns=fullflexible
  }
...


Interpreted Automata
====================

Context
-------

 - Static analysis of C code with \FramaC / \Eva
 - Control Flow partially abstracted by Cil
     -  All structured loops are transformed into `while (true)` loops
     -  Lazy operators are translated to `if-then-else`
     -  Returns are only allowed as the last statement
     -  The `Cfg` module can translate `break`, `continue` and `switch` to
        `if` and `goto`
     -  The analysis still has to deal with "undefined sequences"
     -  ... and new constructs from C++ like `try` - `catch`
 -  Generic dataflow analysis are available (modules `Dataflow2`, `Dataflows`)
     -  It completely abstracts the control-flow for the analyser, but forces
        the logic and the order of a fixpoint iteration


Automates Interprétés : Exemple {.fragile}
-------------------------------

\noindent
\begin{minipage}{.3\textwidth}
\begin{lstlisting}[language=C]
int i = 0, j = 0;
while (i < 10) {
  if (i % 2) {
    j++;
  }
  i++;
}
\end{lstlisting}
\end{minipage}
\begin{minipage}{.3\textwidth}
\begin{tikzpicture}[chainautomata]
\node[cp] (c1) {$c_1$};
\node[cp] (c2) {$c_2$};
\node[cp] (c3) {$c_3$};
\node[cp] (c4) {$c_4$};
\node[cp] (c5) {$c_5$};
\node[cp] (c7) {$c_7$};
\node[cp,right=16mm of c4] (c8) {$c_8$};
\node[cp,right=of c5] (c6) {$c_6$};
\draw
  (c1) edge node[right] {\tt i = 0} (c2)
  (c2) edge node[right] {\tt j = 0} (c3)
  (c3) edge node[right] {\tt i < 10} (c4)
       edge node[above right] {\tt i >= 10} (c8)
  (c4) edge node[left] {\tt i \% 2} (c5)
       edge node[right] {\tt !(i \% 2)} (c6)
  (c5) edge node[left] {\tt j++} (c7)
  (c6) edge node[right] {\tt j += 2} (c7)
  (c7) edge [bend left=90] node[left] {\tt i++} (c3)
;
\end{tikzpicture}
\end{minipage}


Why use an intermediate representation ?
----------------------------------------

 -  An intermediate representation for the control flow graph
     -  discharges the anlyses from reconstructing it
     -  with only light constraints
 -  Suitable for transformations
 -  Independent from source language, as long as transfer functions are
    general enough


Semantics {.fragile}
---------

\newcommand{\abs}[1]{#1^\#}

### Operational Semantics

\noindent
\begin{minipage}{.23\textwidth}
\centering
\begin{tikzpicture}[chainautomata]
\node [cp] (c1) {$c_1$};
\node [cp] (c2) {$c_2$};
\draw
  (c1) edge node[right] {$f$} (c2)
;
\end{tikzpicture}
\end{minipage}
\begin{minipage}{.75\textwidth}
\begin{tabular}{lll}
$f$ is a {\bf guard} :   & $c1,\sigma \longrightarrow c2,\sigma$    & if $f(\sigma)$ \\
$f$ is a {\bf command} : & $c1,\sigma \longrightarrow c2,f(\sigma)$ &
\end{tabular}
\end{minipage}

### Collecting Semantics

\noindent
\begin{minipage}{.23\textwidth}
\centering
\begin{tikzpicture}[chainautomata]
\node [cp] (c1) {$c_1$};
\node [right=2mm of c1] (c2) {$\dots$};
\node [cp,right=2mm of c2] (cn) {$c_n$};
\node [cp,below=5mm of c2] (d) {$d$};
\draw
  (c1) edge node[left] {$F_1$} (d)
  (c2) edge (d)
  (cn) edge node[right] {$F_n$} (d)
;
\end{tikzpicture}
\end{minipage}
\begin{minipage}{.75\textwidth}
\[
  Y = \bigcup_{c_i \rightarrow d} F_i\tikzlabel{tf}(X_i)
\]
\end{minipage}
\uncover<2>{\tikzcomment{tf}{\textwidth,-2ex}{{\it Transfer functions}, in theory could be \\ any monotonic function}}

### Abstract Semantics

\noindent
\begin{minipage}{.23\textwidth}
\centering
\begin{tikzpicture}[chainautomata]
\node [cp] (c1) {$c_1$};
\node [right=2mm of c1] (c2) {$\dots$};
\node [cp,right=2mm of c2] (cn) {$c_n$};
\node [cp,below=5mm of c2] (d) {$d$};
\draw
  (c1) edge node[left] {$\abs{F}_1$} (d)
  (c2) edge (d)
  (cn) edge node[right] {$\abs{F}_n$} (d)
;
\end{tikzpicture}
\end{minipage}
\begin{minipage}{.75\textwidth}
\[
  \abs{Y} = \bigsqcup_{c_i \rightarrow d} \abs{F}_i(\abs{X}_i)
\]
\end{minipage}


Expressivity {.fragile}
------------

### Incompleteness

\centering
\begin{tikzpicture}[chainautomata]
\node[cp] (c1) {$c_1$};
\node[cp] (c2) {$c_2$};
\node[cp] (c3) {$c_3$};
\draw
  (c1) edge node[right] {\tt i = [0..10]} (c2)
  (c2) edge node[right] {\tt i < 5} (c3)
;
\end{tikzpicture}

### Nondeterminism

\centering
\begin{tikzpicture}[chainautomata]
\node[cp] (c1) {$c_1$};
\node[cp] (c2) {$c_2$};
\node[cp,below left=5mm and 3mm of c2] (c3) {$c_3$};
\node[cp,below right=5mm and 3mm of c2] (c4) {$c_4$};
\draw
  (c1) edge node[right] {\tt i = [0..10]} (c2)
  (c2) edge node[left] {\tt i < 7} (c3)
       edge node[right] {\tt i > 3} (c4)
;
\end{tikzpicture}


Weak Topological Order {.fragile}
----------------------

\noindent
\begin{minipage}{.4\textwidth}
\begin{onlyenv}<1-3>
\begin{lstlisting}[language=C]
int A[10][10];
int i = 0;
while (i < 10) {
  int j = 0;
  while (j < 10) {
    A[i][j] = 0;
    j++;
  }
  i++;
}
\end{lstlisting}
\end{onlyenv}
\begin{onlyenv}<4->
\begin{lstlisting}[language=C]
int A[10][10];
int i = 0;
l1: if (i < 10) {
  int j = 0;
  l2 : if (j < 10) {
    A[i][j] = 0;
    j++;
    goto l2;
  }
  i++;
  goto^\tikzlabel{goto}^ l1;
}
\end{lstlisting}
\tikzcomment{goto}{\textwidth,0}{Works the same with \\ {\tt goto} loops}
\end{onlyenv}
\end{minipage}
\begin{minipage}{.5\textwidth}
\centering
\only<1>{\tikzstyle{head} = [cp]}
\only<2->{\tikzstyle{head} = [wp]}
\begin{tikzpicture}[chainautomata,remember picture]
\node[cp] (c1) {$c_1$};
\node[head] (c2) {$c_2$};
\node[cp] (c3) {$c_3$};
\node[head] (c4) {$c_4$};
\node[cp] (c5) {$c_5$};
\node[cp] (c6) {$c_6$};
\node[cp] (c7) {$c_7$};
\node[cp,right=20mm of c3] (c8) {$c_8$};
\draw
  (c1) edge node[right] {\tt i = 0} (c2)
  (c2) edge node[right] {\tt i < 10} (c3)
       edge node[above right] {\tt i >= 10} (c8)
  (c3) edge node[right] {\tt j = 0} (c4)
  (c4) edge node[right] {\tt j < 10} (c5)
  (c5) edge node[right] {\tt A[i][j] = 0} (c6)
  (c6) edge [bend left=45] node[left] {\tt j++} (c4)
  (c7) edge [bend left=70] node[left] {\tt i++} (c2)
;
\draw (c4) .. controls +(2.0, 0) and +(2.0,0.0) .. node[right] {\tt j >= 10} (c7);

\begin{scope}[on background layer]
\uncover<2->{
  \draw[component,fill=white!80!green] ($(c7.south west)-(1.5,0.2)$) rectangle ($(c2.north east)+(1.5,0.1)$);
  \draw[component] ($(c6.south west)-(0.4,0.2)$) rectangle ($(c4.north east)+(0.4,0.1)$);
  \node (scc2) at ($(c4.north east)+(0.4,0.1)$) {};
}
\end{scope}
\end{tikzpicture}
\uncover<2->{
  $(c_1~(c_2~c_3~(c_4~c_5~c_6)~c_7)~c_8)$
}
\end{minipage}
\begin{minipage}{.05\textwidth}
\begin{overlayarea}{\textwidth}{2cm}
\only<3>{
  \tikzcomment{c2}{\textwidth,12ex}{Loop head}
  \tikzcomment{scc2}{\textwidth,4ex}{Strongly connected {\bf sub}-component}
}
\end{overlayarea}
\end{minipage}


WTO properties
--------------

- Rebuild the original structure of the program
- But also works on destructurated programs with non natural loops.
- For abstract interpretation, gives widening points


WTO with priorities {.fragile}
-------------------

\noindent
\begin{minipage}{.55\textwidth}
\begin{lstlisting}[language=C]
start:
  int i = 0;
  //@ loop invariant i <= 10;
  while (true) {
    i++;
    if (i >= 10)
      break;
  }
  goto start;
\end{lstlisting}
\end{minipage}
\begin{minipage}{.40\textwidth}
\tikzstyle{h1} = []
\tikzstyle{h2} = []
\tikzstyle{h3} = []
\only<2>{\tikzstyle{h1} = [wp]}
\only<3>{\tikzstyle{h2} = [wp]}
\only<4>{\tikzstyle{h3} = [wp]}
\begin{tikzpicture}[chainautomata,remember picture]
\node[cp,h1,h3] (c1) {$c_1$};
\node[cp,h1,h2] (c2) {$c_2$};
\node[cp,h3]    (c3) {$c_3$};
\node[cp]       (c4) {$c_4$};
\node[cp]       (c5) {$c_5$};
\draw
  (c1) edge node[right] {\tt i = 0} (c2)
  (c2) edge node[right] {\tt i ++} (c3)
  (c3) edge node[right] {\tt i < 10} (c4)
  (c4) edge [bend left=45] node[left] {} (c2)
  (c5) edge [bend left=70] node[left] {} (c1)
;
\draw (c3) .. controls +(1.5, 0) and +(1.5,0.0) ..  node[right] {\tt i >= 10} (c5);

\begin{scope}[on background layer]
\uncover<2->{
  \draw[component,fill=white!80!green] ($(c5.south west)-(1.5,0.2)$) rectangle ($(c1.north east)+(1.5,0.1)$);
}
\uncover<2,4>{
  \draw[component] ($(c4.south west)-(0.4,0.2)$) rectangle ($(c2.north east)+(0.4,0.1)$);
}
\end{scope}
\end{tikzpicture}
\end{minipage}

\begin{itemize}
\item Several natural choices of loop heads
  \begin{itemize}
  \item \only<2>{\bf} First vertex encountered \normalfont
  \item \only<3>{\bf} Structured loops with invariants \normalfont
  \item \only<4>{\bf} Before loop exits \normalfont
  \end{itemize}
\end{itemize}


Interpreted Automata in Frama-C
===============================

Frama-C Architecture Changes
----------------------------

\begin{center}
\tikzstyle{brique} = [
  rounded corners=2mm,
  draw, semithick,
  blur shadow={shadow blur steps=5},
  minimum height=10mm,
  minimum width=18mm,
  align=center,
  font=\small,
  node distance=5mm
]
\tikzstyle{original} = [brique,fill=white!80!green]
\tikzstyle{changed} = [brique,fill=white!80!yellow]

\begin{tikzpicture}[>=stealth]
  \node (CAbs) [original] {CAbs};
  \node (Cil)  [original,right=of CAbs] {Cil};
  \node (Cfg)  [original,right=of Cil] {Cfg};
  \node (Dtf)  [original,right=of Cfg] {Dataflow};
  \node (M1)   [above right=of Dtf] {\dots};

  \draw[->] (CAbs.east) to (Cil.west);
  \draw[->] (Cil.east) to (Cfg.west);
  \draw[->] (Cfg.east) to (Dtf.west);
  \draw[->] (Dtf.east) to (M1.west);

  \uncover<1>{
    \node (PD)  [original,right=of Dtf] {Eva};
    \node (WP)  [original,below right=6mm and 6mm of Cfg] {WP};

    \draw[->] (Dtf.east) to (PD.west);
    \draw[->] (Cfg.east) to (WP.west);
  }

  \uncover<2>{
    \draw[dashed] (Cfg.south west) -- ($(Cfg.south west)-(0,0.6)$);
    \draw[dashed] (Cfg.south east) -- ($(Cfg.south east)-(0,0.6)$);
    \draw[dashed] (Dtf.south west) -- ($(Dtf.south west)-(0,0.6)$);
    \draw[dashed] (Dtf.south east) -- ($(Dtf.south east)-(0,0.6)$);

    \node (M2)  [right=of Dtf] {\dots};
    \node (IA)  [changed,below right=6mm and 5mm of Cil, minimum width=28mm] {Interpreted \\ Automata};
    \node (PD') [changed,right=of IA] {Eva};
    \node (WP') [changed,below right=6mm and 5mm of IA] {WP};

    \draw[->] (Dtf.east) to (M2.west);
    \draw[->] (Cil.east) to (IA.west);
    \draw[->] (IA.east)  to (PD'.west);
    \draw[->] (IA.east) to (WP'.west);
  }
\end{tikzpicture}
\end{center}


Why use interpreted automata in Eva
-----------------------------------

 -  Simplifies the iteration engine
     -  Simpler control flow, does not go in and out `Dataflow2` functor
     -  Automatic handling of Enter / Leave transitions
 -  Total liberty in the order of evaluation
     -  Widening points are now synchronized with the iteration strategies
     -  Easy implementation of descending sequences
     -  Easy implementation of a "reset sub-components before iteration"
 -  The graph structure can be reused to store the result of the analysis more
    precisely (Not done yet)


Ocaml Definition 1/2
--------------------

\lstset{basicstyle=\scriptsize\ttfamily}
````ocaml
type guard_kind = Then | Else
type assert_kind = Invariant | Assert | Check | Assume 

type 'vertex transition =
  | Skip
  | Return of exp option * stmt
  | Guard of exp * guard_kind * stmt
  | Prop of assert_kind * identified_predicate * guard_kind *
      'vertex labels * Property.t
  | Instr of instr * stmt
  | Enter of block
  | Leave of block

type 'vertex edge = private {
  edge_key : int;
  edge_kinstr : kinstr;
  edge_transition : 'vertex transition;
  edge_loc : location;
}
````

Ocaml Definition 2/2
--------------------

\lstset{basicstyle=\scriptsize\ttfamily}

````ocaml
type info = NoneInfo | LoopHead of int (* level *)

type vertex = private {
  vertex_key : int;
  mutable vertex_start_of : stmt option;
  mutable vertex_info : info;
}

module G : Graph.Sig.I
  with type V.t = vertex
  and  type E.t = vertex * vertex edge * vertex
  and  type V.label = vertex
  and  type E.label = vertex edge

type automaton = {
  graph : G.t;
  entry_point : vertex;
  return_point : vertex;
  stmt_table : (vertex * vertex) Cil_datatype.Stmt.Hashtbl.t;
}
````


Architectural choices
---------------------

 -  Only one transfer function per edge
 -  Keep a lot of intermediate vertices: a single statement can lead to several
    vertices
 -  Traceability
     -  Each vertex is associated to the statement it is the start to
     -  Each statement is associated to the two vertex starting and ending
        the statement


Wto in Frama-C
--------------

 -  Implementation in `src/utils/wto.ml`
     -  Original version from Loïc Correnson in 2015
     -  Functorized version from Matthieu Lemerre in 2016
     -  Improved with preferences by François Bobot in 2017

````ocaml
type 'n component =
  | Component of 'n * 'n partition
  | Node of 'n

and 'n partition = 'n component list
````


Fixpoint calculus with WTO
--------------------------

````ocaml
let process_vertex (v : vertex) : bool =
  ...

let rec iterate_list (l : wto) =
  List.iter iterate_element l
and iterate_element = function
  | Wto.Node v ->
      ignore (process_vertex v)
  | Wto.Component (v, w) as component ->
      while process_vertex v do
        iterate_list w;
      done

let wto = Interpreted_automata.get_wto kf in
iterate_list wto
````


Future improvements {.fragile}
-------------------

\lstset{basicstyle=\tiny\ttfamily}

\centering

### Function contracts, logic

\noindent
\begin{minipage}{.45\textwidth}
\begin{lstlisting}[language=C]
/*@ behavior B1:
      assimes A1;
      requires R1;
    behavior B2:
      assumes A2;
      requires R2; */
void f() {
  ...
}
\end{lstlisting}
\end{minipage}
\begin{minipage}{.45\textwidth}
\begin{tikzpicture}[chainautomata,scale=0.7,every node/.style={scale=0.7},node distance=2.5mm]
\node[cp] (c1) {$c_1$};
\node[cp] (c2) {$c_2$};
\node[cp] (c3) {$c_3$};
\node[cp] (c4) {$c_4$};
\node[cp] (c5) {$c_5$};
\draw
  (c1) edge node[right] {$A_1$} (c2)
       edge[bend right] node[left] {$\neg A_1$} (c3)
  (c2) edge node[right] {$R_1$} (c3)
  (c3) edge node[right] {$A_2$} (c4)
       edge[bend right] node[left] {$\neg A_2$} (c5)
  (c4) edge node[right] {$R_2$} (c5)
;
\end{tikzpicture}
\end{minipage}

### Exception handling

\noindent
\begin{minipage}{.45\textwidth}
\begin{lstlisting}[language=C]
void f()
{
  try {
    g();
  }
  catch (...) {
    throw;
  }
}
\end{lstlisting}
\end{minipage}
\begin{minipage}{.45\textwidth}
\begin{tikzpicture}[chainautomata,scale=0.7,every node/.style={scale=0.7},node distance=2.5mm]
\node[cp] (c1) {$c_1$};
\node[cp] (c2) {$c_2$};
\node[cp] (c3) {$c_3$};
\node[cp] (c4) {$c_4$};
\node[cp,right=1cm of c3] (c5) {$c_5$};
\draw
  (c1) edge node[right] {\tt f()} (c2)
  (c2) edge node[right] {\tt catch?} (c3)
       edge node[above right] {$\neg$ \tt catch?} (c5)
  (c3) edge node[right] {\tt throw e} (c4)
;
\end{tikzpicture}
\end{minipage}


More features using automata indirectly
=======================================

Descending sequences {.fragile}
--------------------

\noindent
\begin{minipage}{.35\textwidth}
\begin{lstlisting}[language=C]
void main()
{
  int n = 10;
  int i = 0;
  while (++i < n) {
    ...
  }
}
\end{lstlisting}
\end{minipage}
\begin{onlyenv}<2->
\begin{minipage}{.6\textwidth}
\only<-2>{\tikzstyle{exit} = []}
\only<3->{\tikzstyle{exit} = [thick,draw=blue]}
\begin{tikzpicture}[chainautomata]
\node[cp] (c1) {$c_1$};
\node[cp] (c2) {$c_2$};
\node[wp,exit] (c3) {$c_3$};
\node[cp,exit] (c4) {$c_4$};
\node[cp] (c5) {$c_5$};
\node[cp] (c6) {$c_6$};
\draw
  (c1) edge node[right] {\tt n = 10} (c2)
  (c2) edge node[right] {\tt i = 0} (c3)
  (c3) edge[exit] node[right] {\tt i = i + 1} (c4)
  (c4) edge node[right] {\tt i < n} (c5)
  (c5) edge [bend left=90] node[left] {} (c3)
;
\draw[exit] (c4) .. controls +(1.2, 0) and +(1.2,0.0) ..  node[right] {\tt i >= n} (c6);

\begin{scope}[on background layer]
\draw[component,fill=white!80!green] ($(c5.south west)-(0.7,0.2)$) rectangle ($(c3.north east)+(0.7,0.1)$);
\end{scope}
\node[right=20mm of c1] {1st};
\node[right=20mm of c2] {$[0,0]$};
\node[right=20mm of c3] {$[0,0]$};
\node[right=20mm of c4] {$[1,1]$};
\node[right=20mm of c5] {$[1,1]$};
\node[right=32mm of c1] {2nd};
\node[right=32mm of c3] {$[0,1]$};
\node[right=32mm of c4] {$[1,2]$};
\node[right=32mm of c5] {$[1,2]$};
\node[right=44mm of c1] {3rd};
\node[right=44mm of c3] {$[0,+\infty[$};
\node[right=44mm of c4] {$[1,+\infty[$};
\node[right=44mm of c5] {$[1,9]$};
\node[right=44mm of c6] {$[10,+\infty[$};
\only<3->{
\node[right=56mm of c1] {D};
\node[right=56mm of c3] {$[0,9]$};
\node[right=56mm of c4] {$[1,10]$};
\node[right=56mm of c6] {$[10,10]$};
}
\end{tikzpicture}
\end{minipage}
\end{onlyenv}

\begin{onlyenv}<1>
\lstset{basicstyle=\tiny\ttfamily}
\begin{lstlisting}
test.c:5:[value:alarm] ^\bfseries warning: signed overflow. assert i + 1 $\leq$ 2147483647^;
[value] ====== VALUES COMPUTED ======
[value:final-states] Values at end of function main:
  n ^$\in$^ {10}
  ^\bfseries i $\in$ [10..2147483647]^
\end{lstlisting}
\end{onlyenv}


Loop unrolling {.fragile}
--------------

\noindent
\begin{minipage}{.4\textwidth}
\begin{lstlisting}[language=C]
/*@ slevel ^\only<1>{5}\only<2>{6}\only<3>{0}^; */

int i = 0;
while (i < 5) {
  if (nondet)
    break;
  i++; 
}
int j = 0;

^\uncover<3>{/*@ loop unroll 4; */}^
while (j < 4) {
  A[j] = 0;
  j++;
}
\end{lstlisting}
\end{minipage}
\begin{minipage}{.4\textwidth}
\tikzstyle{as} = [
  circle,
  fill=orange,
  inner sep=0.2pt,
  minimum size=6pt,
  on chain=2,
  node distance=1mm,
  font=\tiny]
\tikzstyle{us} = [as,minimum size=8pt]
\tikzstyle{c0} = [fill=orange]
\tikzstyle{c1} = [fill=red]
\tikzstyle{c2} = [fill=purple]
\tikzstyle{c3} = [fill=blue]
\tikzstyle{c4} = [fill=cyan]
\tikzstyle{c5} = [fill=green]
\tikzstyle{c6} = [fill=black,font=\tiny\color{white}]

\begin{tikzpicture}[chainautomata,node distance=3.5mm,start chain=2 going right]
\node[cp] (c1) {$c_1$};
\node[cp] (c2) {$c_2$};
\node[cp] (c3) {$c_3$};
\node[cp] (c4) {$c_4$};
\node[cp] (c5) {$c_5$};
\node[cp] (c6) {$c_6$};
\node[cp] (c7) {$c_7$};
\node[cp] (c8) {$c_8$};
\node[cp,right=28mm of c7] (c9) {$c_9$};

\draw
  (c1) edge node[right] {\tt i = 0} (c2)
  (c2) edge node[right] {\tt i < 5} (c3)
  (c3) edge node[right] {\tt nondet} (c4)
  (c4) edge[bend left=70] node[left] {\tt i++} (c2)
  (c5) edge node[left] {\tt j = 0} (c6)
  (c6) edge node[right] {\tt j < 4} (c7)
       edge node[above right] {\tt j >= 4} (c9)
  (c7) edge node[right] {\tt A[j] = 0} (c8)
  (c8) edge [bend left=70] node[left] {\tt j++} (c6)
;
\draw (c3) .. controls +(3.0, 0) and +(3.0,0.0) .. node[right] {$\neg$ \tt nondet} (c5);

\tikzset{node distance=1mm}

\only<1>{
  \node[as, c0, right=of c1] {};

  \node[as, c0, right=of c2] {};
  \node[as, c1] {};
  \node[as, c2] {};
  \node[as, c3] {};
  \node[as, c4] {};
  \node[us, c5] {};

  \node[as, c0, right=of c3] {};
  \node[as, c1] {};
  \node[as, c2] {};
  \node[as, c3] {};
  \node[as, c4] {};
  \node[us, c5] {};

  \node[as, c0, right=of c4] {};
  \node[as, c1] {};
  \node[as, c2] {};
  \node[as, c3] {};
  \node[as, c4] {};

  \node[us, c6, right=of c5] {};

  \node[as, c6, right=of c6] {0};
  \node[as, c6] {1};
  \node[as, c6] {2};
  \node[as, c6] {3};
  \node[as, c6] {4};

  \node[as, c6, right=of c7] {0};
  \node[as, c6] {1};
  \node[as, c6] {2};
  \node[as, c6] {3};

  \node[as, c6, right=of c8] {0};
  \node[as, c6] {1};
  \node[as, c6] {2};
  \node[as, c6] {3};

  \node[as, c6, right=of c9] {4};
}

\only<2>{
  \node[as, c0, right=of c1] {};

  \node[as, c0, right=of c2] {};
  \node[as, c1] {};
  \node[as, c2] {};
  \node[as, c3] {};
  \node[as, c4] {};
  \node[as, c5] {};

  \node[as, c0, right=of c3] {};
  \node[as, c1] {};
  \node[as, c2] {};
  \node[as, c3] {};
  \node[as, c4] {};

  \node[as, c0, right=of c4] {};
  \node[as, c1] {};
  \node[as, c2] {};
  \node[as, c3] {};
  \node[as, c4] {};

  \node[as, c0, right=of c5] {};
  \node[as, c1] {};
  \node[as, c2] {};
  \node[as, c3] {};
  \node[as, c4] {};
  \node[as, c5] {};

  \node[as, c0, right=of c6] {0};
  \node[as, c1] {0};
  \node[as, c2] {0};
  \node[as, c3] {0};
  \node[as, c4] {0};
  \node[as, c5] {0};
  \node[us, c6] {*};

  \node[as, c0, right=of c7] {0};
  \node[as, c1] {0};
  \node[as, c2] {0};
  \node[as, c3] {0};
  \node[as, c4] {0};
  \node[as, c5] {0};
  \node[us, c6] {*};

  \node[as, c0, right=of c8] {0};
  \node[as, c1] {0};
  \node[as, c2] {0};
  \node[as, c3] {0};
  \node[as, c4] {0};
  \node[as, c5] {0};
  \node[us, c6] {*};

  \node[as, c6, right=of c9] {4};
}

\only<3>{
  \node[us, c6, right=of c1] {};
  \node[us, c6, right=of c2] {};
  \node[us, c6, right=of c3] {};
  \node[us, c6, right=of c4] {};
  \node[us, c6, right=of c5] {};

  \node[us, c6, right=of c6] {0};
  \node[us, c6] {1};
  \node[us, c6] {2};
  \node[us, c6] {3};
  \node[us, c6] {4};

  \node[us, c6, right=of c7] {0};
  \node[us, c6] {1};
  \node[us, c6] {2};
  \node[us, c6] {3};

  \node[us, c6, right=of c8] {0};
  \node[us, c6] {1};
  \node[us, c6] {2};
  \node[us, c6] {3};

  \node[us, c6, right=of c9] {4};
}

\end{tikzpicture}
\end{minipage}

