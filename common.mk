DIR := $(dir $(lastword $(MAKEFILE_LIST)))
PANDOC_DATA := $(DIR)/pandoc-data
export TEXINPUTS=.:$(DIR)/latex/CeaReport:$(DIR)/latex/CeaBeamer:$(DIR)/latex:

define echo_target
      @tput setaf 4
      @echo $@
      @tput sgr0
endef

.PHONY: all clean

all:

clean::
	$(RM) --recursive *.pdf latexmk

slides-%.tex slides-%.pdf: slides-%.md
	$(call echo_target)
	pandoc --from markdown --to beamer \
	  --data-dir $(PANDOC_DATA) --listings --slide-level 2 \
	  -o $@ $(filter %.md %.yaml,$^)

rapport-%.tex rapport-%.pdf: rapport-%.md
	$(call echo_target)
	pandoc --from markdown --to latex \
	  --data-dir $(PANDOC_DATA) --listings \
		-o $@ $<

%.pdf: %.tex
	$(call echo_target)
	@texfot --quiet --no-stderr latexmk -use-make -outdir=latexmk -pdf $*
	@cp --force "latexmk/$@" "$@"

%.png: %.pdf
	$(call echo_target)
	convert -density 300 $< $@

%.tex: %.md %.sed
	$(call echo_target)
	pandoc --from markdown --to latex --listings $< -o $@
	sed --file=$*.sed --in-place $@
