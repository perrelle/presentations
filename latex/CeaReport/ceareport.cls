% LaTeX class for technical reports
% Author: Virgile Prevosto
% Usage: \documentclass{ceareport}\begin{document}\maketitle...\end{document}
%
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ceareport}[2017/01/25]

\newcommand{\@lab}{Lxy}
\newcommand{\@lead}{}

\DeclareOption{LSL}{
  \renewcommand{\@lab}{LSL}
  \renewcommand{\@lead}{Florent Kirchner}
}

\DeclareOption{LISE}{
  \renewcommand{\@lab}{LISE}
  \renewcommand{\@lead}{Sébastien Gérard}
}

\ProcessOptions

\LoadClass[a4paper]{report}
\RequirePackage{pgf}
\RequirePackage{calc}
\PassOptionsToPackage{absolute}{textpos}
\RequirePackage{textpos}
\RequirePackage{times}
\RequirePackage{color}
\RequirePackage{colortbl}
\RequirePackage{fancyhdr}
\RequirePackage{lastpage}
\RequirePackage{tabularx}
\RequirePackage{hhline}
\RequirePackage{etoolbox}
\RequirePackage{tocloft}
\PassOptionsToPackage{top=2.5cm,bottom=2.5cm,left=2.5cm,right=2.5cm}{geometry}
\RequirePackage{geometry}
\PassOptionsToPackage{utf8x}{inputenc}
\RequirePackage{inputenc}
\PassOptionsToPackage{T1}{fontenc}
\RequirePackage{fontenc}
\PassOptionsToPackage{french,english}{babel}
\RequirePackage{babel}


\pgfdeclareimage[width=6.57cm]{logo-list-ceatech}{logo-list-ceatech}
\pgfdeclareimage[width=1.8cm]{carnot}{carnot-upsaclay}
\pgfdeclareimage[width=1.4cm]{universite-paris-saclay}{logo-universite-paris-saclay}
\pgfdeclareimage[width=0.6cm]{barre-gradient-vert}{barre-gradient-vert}

\newlength{\titlespace}

\newcommand{\@evolutions}{}
\newcommand{\addevolution}[4]{%
  \gpreto{\@evolutions}{%
    #1 & #2 & #3 & #4
    \\
    \hline}
}

\newcommand{\@diffusion}{}
\newcommand{\diffusion}[1]{\renewcommand{\@diffusion}{#1}}

\definecolor{ceagrey}{gray}{0.3}
\definecolor{ceaorange}{rgb}{1.0,0.5,0.0}
\definecolor{ceagreen}{rgb}{0.4,0.8,0.0}
\definecolor{textgrey}{gray}{0.5}

\newcommand{\@ref}{}
\newcommand{\reference}[1]{\renewcommand{\@ref}{#1}}

\newcommand{\@englishtitle}{\@title}
\newcommand{\englishtitle}[1]{\renewcommand{\@englishtitle}{#1}}

\newcommand{\@keywords}{}
\newcommand{\keywords}[1]{\renewcommand{\@keywords}{#1}}

\newcommand{\@affaire}{}
\newcommand{\affaire}[1]{\renewcommand{\@affaire}{#1}}

\newcommand{\@projet}{}
\newcommand{\projet}[1]{\renewcommand{\@projet}{#1}}

\newcommand{\@identifieur}{}
\newcommand{\identifieur}[1]{\renewcommand{\@identifieur}{#1}}

\newcommand{\@chefprojet}{}
\newcommand{\chefprojet}[1]{\renewcommand{\@chefprojet}{#1}}

\newcommand{\@dilswriters}{\@author \ifdefempty{\@chefprojet}{}{\\ \@chefprojet}}
\newcommand{\dilswriters}[1]{\renewcommand{\@dilswriters}{#1}}

\newcommand{\@controller}{\@lead}
\newcommand{\controller}[1]{\renewcommand{\@controller}{#1}}

\newcommand{\@abstract}{}
\renewcommand{\abstract}[1]{\renewcommand{\@abstract}{#1}}

\newcommand{\disclaimer}{
  \begin{textblock*}{21cm}(0cm,28cm)
    \fontfamily{ptm}\fontsize{7pt}{8pt}\selectfont\centering\textcolor{ceagrey}{Ce document et les informations qu'il contient, sont la propriété exclusive du CEA. Ils ne peuvent pas être communiqués \\
      ou divulgués sans une autorisation préalable du CEA LIST}
  \end{textblock*}
}

\newcommand{\ceaorangebar}{\textcolor{ceaorange}{$\mathbf{|}$ }}
\newcommand{\ceagreenbar}{\textcolor{ceagreen}{$\mathbf{|}$ }}

\newcommand{\adressecea}{\begin{minipage}[b]{\textwidth}
    \setlength{\baselineskip}{0pt}
    \pgfuseimage{barre-gradient-vert}~\\
    \fontfamily{phv}\fontsize{8pt}{9pt}\selectfont{}%
    \color{textgrey}
    Commissariat à l’énergie atomique et aux énergies alternatives~\qquad{}~\qquad{}DILS \\
    Institut List \ceaorangebar
    CEA Saclay Nano-INNOV \ceaorangebar
    Bât. 862-PC174 \\
    91 191 Gif-sur-Yvette Cedex - FRANCE \\
    T. +33 1 69 08 45 19 \\
    \ceagreenbar \textcolor{ceaorange}{\bf www-list.cea.fr} \\
    \fontfamily{phv}\fontsize{7pt}{8pt}\selectfont{}%
    Établissement public à caractère industriel et commercial \ceagreenbar
    RCS Paris B~775~685~019
\end{minipage}}

\fancypagestyle{plain}{%
  \lfoot{}
  \cfoot{\vskip 6pt\disclaimer}
  \rfoot{\thepage/\pageref{LastPage}}
  \renewcommand{\headrulewidth}{0pt}
  \renewcommand{\footrulewidth}{0.6pt}
  \chead{}
  \lhead{}
  \rhead{}
}

\renewcommand{\contentsname}{Table des matières}
\renewcommand{\cfttoctitlefont}{\normalsize\hfill}
\renewcommand{\cftaftertoctitle}{\hfill}
\newsavebox{\titlebox}
\newsavebox{\authorbox}
\renewcommand{\maketitle}{
  \thispagestyle{fancy}
  %\lhead{\vskip-20pt\vbox to 50pt{\hskip-110pt\pgfuseimage{logocea}}}
  \begin{textblock*}{6.57cm}(2cm,3.25cm) % as measured on the 2016 template
    \pgfuseimage{logo-list-ceatech}
  \end{textblock*}
  %\begin{textblock*}{6cm}(4.5cm,1cm) % as measured on the 2012 template
  %  \raggedright\pgfuseimage{logolist}\\
  %  \fontfamily{phv}\fontsize{6pt}{6pt}\selectfont{}CEA/SACLAY\newline
  %  DIRECTION DE LA RECHERCHE TECHNOLOGIQUE\newline
  %  DEPARTEMENT INGENIERIE LOGICIELS ET SYSTEMES
  %\end{textblock*}
  \rhead{}
  \cfoot{\vskip 6pt\disclaimer}
  \renewcommand{\headrulewidth}{0pt}
  ~
  \vskip 260pt
  \begin{center}
    \textbf{\Large Département Ingénierie Logiciels et Systèmes}\par
    \vskip 44pt
    \makebox[\textwidth][r]{\textsf{Saclay, le \@date}}
    \vskip 35pt
    \setlength{\titlespace}{126pt}
    \sbox{\titlebox}{\parbox{\textwidth}{\centering\huge\textit{\it\@title}}}
    \usebox{\titlebox}
    \setlength{\titlespace}{\titlespace-\totalheightof{\usebox{\titlebox}}}
    \vskip 14pt
    \sbox{\authorbox}{\parbox{\textwidth}{\centering\textbf{Par}~\textbf{\@author}}}
    \usebox{\authorbox}\\
    \setlength{\titlespace}{\titlespace-\totalheightof{\usebox{\authorbox}}}
    \textbf{DRT/LIST/DILS/\@lab~(CEA)}
    \vskip\titlespace
  \end{center}
  \begin{textblock*}{21cm}(4.3cm,25.5cm) % as measured on the 2016 template
    \adressecea
  \end{textblock*}
  \begin{textblock*}{1.8cm}(15.6cm,26.7cm) % as measured on the 2016 template
    \pgfuseimage{carnot}
  \end{textblock*}
  \begin{textblock*}{1.4cm}(17.5cm,26.7cm) % as measured on the 2016 template
    \pgfuseimage{universite-paris-saclay}
  \end{textblock*}

}
